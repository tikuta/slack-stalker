#! /usr/bin/env python
# coding=utf-8

import subprocess
from string import Template
from slacker import Slacker
import time


def notify(title, subtitle, content):
    tmpl = Template("""echo 'display notification "${content}" with title "${title}" subtitle "${subtitle}"' | osascript""")
    subprocess.call(tmpl.safe_substitute(locals()), shell=True)


def loop(presence):
    presence.loop()


class Presence():
    def __init__(self, key, name):
        self.key = key
        self.name = name
        self.uid = self.get_uid()
        self.state = self.get_presence()
        self.notify()

    def get_uid(self):
        slack = Slacker(self.key)
        response = slack.users.list()
        uid = None
        for member in response.body['members']:
            if member['name'] == self.name:
                uid = member['id']
                break
        if not uid:
            raise Exception('no user named %s' % self.name)
        return uid

    def get_presence(self):
        slack = Slacker(self.key)
        response = slack.users.get_presence(self.uid)
        return response.body['presence'] == 'active'

    def loop(self):
        new_state = self.get_presence()

        print(new_state)

        if new_state != self.state:
            self.state = new_state
            self.notify()

    def notify(self):
        notify('slack-stalker', self.name, 'now active' if self.state else 'now away')


def main():
    key = 'xoxp-2643013207-4366056676-4689172553-94d910'
    name = 'mtakemoto'
    presence = Presence(key, name)

    while True:
        time.sleep(10 * 60)  # every 10 min
        presence.loop()

if __name__ == '__main__':
    main()